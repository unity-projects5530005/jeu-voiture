using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class global : MonoBehaviour
{
    public float speed;
    public TMP_Text textMeshPro;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void MoveCube()
    {
        // R�cup�re les positions et cr�e un objet Vector
        float postionX = GameObject.Find("Cube").transform.position.x + this.speed;
        float postionY = GameObject.Find("Cube").transform.position.y;
        float postionZ = GameObject.Find("Cube").transform.position.z;
        Vector3 postion = new Vector3(postionX, postionY, postionZ);
        // Attribue les nouvelles positions � l'objet
        GameObject.Find("Cube").transform.position = postion;
    }

    public void RenameText()
    {
        // Modifie le libell� du text
        this.textMeshPro.text = "X = " + GameObject.Find("Cube").transform.position.x;
    }
}
